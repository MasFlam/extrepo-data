Source: extrepo-data
Section: admin
Priority: optional
Maintainer: Thomas Goirand <zigo@debian.org>
Uploaders:
 Wouter Verhelst <wouter@debian.org>,
Build-Depends:
 debhelper-compat (= 11),
 libcryptx-perl,
 libdistro-info-perl,
 libyaml-libyaml-perl,
Standards-Version: 4.6.0
Vcs-Git: https://salsa.debian.org/extrepo-team/extrepo-data.git
Vcs-Browser: https://salsa.debian.org/extrepo-team/extrepo-data
Homepage: https://salsa.debian.org/extrepo-team/extrepo

Package: extrepo-offline-data
Architecture: all
Depends:
 ${misc:Depends},
Description: External repository manager
 External repositories are additional software package repositories that
 are not maintained by Debian. Before extrepo, maintainers of such
 repositories would suggest that you download and execute an (unsigned)
 shell script as root, or that you download and install their (unsigned)
 package, which is not ideal for security.
 .
 The extrepo package tries to remedy this, by providing a curated list
 of external repositories that can be enabled by a simple command,
 allowing unsigned scripts to be replaced by a simple "extrepo enable
 example.com_repo".
 .
 Note, however, that while the repositories are curated, and that any
 repositories with malicious content will be removed and/or disabled
 when detected, no warranty is made by the Debian project as to the
 security or quality of the software in these third-party repositories.
 .
 This package contains all of the repository offline data, to be used if your
 computer isn't connected to internet.
